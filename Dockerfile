FROM maven:3.6.0-alpine as build

COPY . /tmp

RUN cd tmp && mvn clean install -DskipTests

FROM openjdk:8-alpine

COPY --from=build /tmp/target/serving.jar /usr/app/app.jar

RUN apk add --no-cache curl

ENV PORT=8080
EXPOSE 8080

CMD java -jar -Dserver.port=${PORT} /usr/app/app.jar