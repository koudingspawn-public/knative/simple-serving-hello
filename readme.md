# Installation

## Knative

```bash
$ curl -L https://github.com/knative/serving/releases/download/v0.2.3/serving.yaml \
    | sed 's/LoadBalancer/ClusterIP/' \
    | sed '/nodePort:/d' \
    | sed '/--statsdUdpAddress/d' \
    | sed '/istio-statsd-prom-bridge/d' \
    | kubectl apply -f -
```
    
## Function

```bash
$ kubectl apply -f kubernetes/serving.yaml
$ kubectl apply -f kubernetes/allow-minio.yaml
```

Make the function accessible via DNS entry simple-serving-java.simple-serving.example.com
This one is accessible via:

```bash
$ kubectl get ksvc -n simple-serving
NAME                  DOMAIN                                           LATESTCREATED               LATESTREADY                 READY   REASON
simple-serving-java   simple-serving-java.simple-serving.example.com   simple-serving-java-00001   simple-serving-java-00001   False   RevisionFailed
```
or
```bash
$ kubectl get ksvc -n simple-serving simple-serving-java --template="{{ .status.domain }}"
```

Deploy second revision with another scaling of images with 50% routing to newer version and 50% to old version.

```bash
$ kubectl apply -f kubernetes/serving-50percent.yaml
```

## Minio

```bash
$ kubectl apply -f minio/deployment.yaml
```