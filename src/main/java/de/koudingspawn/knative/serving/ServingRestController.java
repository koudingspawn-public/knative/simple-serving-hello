package de.koudingspawn.knative.serving;

import de.koudingspawn.knative.serving.image.event.ImageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
public class ServingRestController {

    private final Logger log = LoggerFactory.getLogger(ServingRestController.class);

    private final ServingService servingService;

    public ServingRestController(ServingService servingService) {
        this.servingService = servingService;
    }

    @PostMapping("/webhook/image")
    public void minio(@RequestBody ImageEvent imageEvent) {
        log.info("Received webhook: {}", imageEvent.toString());
        servingService.handleImageEvent(imageEvent);

    }

}
