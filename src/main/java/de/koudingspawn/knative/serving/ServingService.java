package de.koudingspawn.knative.serving;

import de.koudingspawn.knative.serving.image.ImageResizerService;
import de.koudingspawn.knative.serving.image.event.ImageEvent;
import de.koudingspawn.knative.serving.image.event.ImageEventRecord;
import de.koudingspawn.knative.serving.s3.S3Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

@Service
public class ServingService {

    private static final Logger log = LoggerFactory.getLogger(ServingService.class);

    private final S3Service s3Service;
    private final ImageResizerService imageResizerService;
    private final String targetBucket;

    public ServingService(S3Service s3Service,
                          ImageResizerService imageResizerService,
                          @Value("${aws.s3.targetbucket}") String targetBucket) {
        this.s3Service = s3Service;
        this.imageResizerService = imageResizerService;
        this.targetBucket = targetBucket;
    }

    public void handleImageEvent(ImageEvent event) {

        for (ImageEventRecord record : event.getRecords()) {

            String bucketName = record.getS3().getBucket().getName();
            String fileName = record.getS3().getObject().getKey();

            Optional<InputStream> inputStream = s3Service.receiveFile(bucketName, fileName);

            if (inputStream.isPresent()) {
                Optional<BufferedImage> resizedImage = imageResizerService.resize(inputStream.get());
                uploadImage(fileName, resizedImage);
            }

        }
    }

    private void uploadImage(String fileName, Optional<BufferedImage> resizedImage) {
        if (resizedImage.isPresent()) {

            try {
                s3Service.saveFile(targetBucket, fileName, resizedImage.get());
            } catch (IOException e) {
                log.error("Failed to upload file to S3 Bucket", e);
            }

        }
    }

}
