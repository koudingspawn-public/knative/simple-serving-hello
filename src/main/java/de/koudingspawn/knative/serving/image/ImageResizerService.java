package de.koudingspawn.knative.serving.image;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

@Service
public class ImageResizerService {

    private final double resizeFactor;

    private static final Logger log = LoggerFactory.getLogger(ImageResizerService.class);

    public ImageResizerService(@Value("${resize.factor}") double resizeFactor) {
        this.resizeFactor = resizeFactor;
    }

    public Optional<BufferedImage> resize(InputStream in) {
        try {
            BufferedImage imageBuf = ImageIO.read(in);

            int targetHeight = (int) (imageBuf.getHeight() * resizeFactor);
            int targetWidth = (int) (imageBuf.getWidth() * resizeFactor);

            BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, imageBuf.getType());
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(imageBuf, 0, 0, targetWidth, targetHeight, null);
            g.dispose();

            return Optional.of(resizedImage);
        } catch (IOException e) {
            log.error("Failed to resize image with exception", e);

            return Optional.empty();
        }


    }

}
