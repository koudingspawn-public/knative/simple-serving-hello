package de.koudingspawn.knative.serving.image.event;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ImageEvent {

    @JsonProperty("EventName")
    private String eventName;
    @JsonProperty("Key")
    private String key;
    @JsonProperty("Records")
    private List<ImageEventRecord> records;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<ImageEventRecord> getRecords() {
        return records;
    }

    public void setRecords(List<ImageEventRecord> records) {
        this.records = records;
    }

    @Override
    public String toString() {
        return "ImageEvent{" +
                "eventName='" + eventName + '\'' +
                ", key='" + key + '\'' +
                ", records=" + records +
                '}';
    }
}
