package de.koudingspawn.knative.serving.image.event;

import java.time.LocalDateTime;

public class ImageEventRecord {

    private LocalDateTime localDateTime;
    private ImageEventRecordS3 s3;

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public ImageEventRecordS3 getS3() {
        return s3;
    }

    public void setS3(ImageEventRecordS3 s3) {
        this.s3 = s3;
    }

    @Override
    public String toString() {
        return "ImageEventRecord{" +
                "localDateTime=" + localDateTime +
                ", s3=" + s3 +
                '}';
    }
}
