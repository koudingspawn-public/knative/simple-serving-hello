package de.koudingspawn.knative.serving.image.event;

public class ImageEventRecordS3 {

    private ImageEventRecordS3Bucket bucket;
    private ImageEventRecordS3Object object;

    public ImageEventRecordS3Bucket getBucket() {
        return bucket;
    }

    public void setBucket(ImageEventRecordS3Bucket bucket) {
        this.bucket = bucket;
    }

    public ImageEventRecordS3Object getObject() {
        return object;
    }

    public void setObject(ImageEventRecordS3Object object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return "ImageEventRecordS3{" +
                "bucket=" + bucket +
                ", object=" + object +
                '}';
    }
}
