package de.koudingspawn.knative.serving.image.event;

public class ImageEventRecordS3Bucket {

    private String name;
    private String arn;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArn() {
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }

    @Override
    public String toString() {
        return "ImageEventRecordS3Bucket{" +
                "name='" + name + '\'' +
                ", arn='" + arn + '\'' +
                '}';
    }
}
