package de.koudingspawn.knative.serving.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

@Service
public class S3Service {

    private final AmazonS3 client;

    public S3Service(AmazonS3 client) {
        this.client = client;
    }

    public Optional<InputStream> receiveFile(String bucket, String fileName) {
        if (this.client.doesObjectExist(bucket, fileName)) {
            return Optional.of(this.client.getObject(bucket, fileName).getObjectContent());
        }

        return Optional.empty();
    }

    public void saveFile(String bucket, String fileName, BufferedImage image) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg" , os);
        byte[] buffer = os.toByteArray();
        InputStream inputStream = new ByteArrayInputStream(buffer);

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(buffer.length);
        this.client.putObject(bucket, fileName, inputStream, metadata);
    }

}
